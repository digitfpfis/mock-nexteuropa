Mock NextEuropa
---------------

This is a mocked version of the NextEuropa platform that can be used for developing Phing build scripts. Once the NextEuropa code has been migrated to git we can use that instead and this will become obsolete.

References:
-----------
* https://bitbucket.org/digitfpfis/subsite-starterkit/
* https://webgate.ec.europa.eu/CITnet/jira/browse/NEXTEUROPA-2539
* https://webgate.ec.europa.eu/CITnet/jira/browse/NEXTEUROPA-2472
* https://webgate.ec.europa.eu/CITnet/jira/browse/NEXTEUROPA-2461
